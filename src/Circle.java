public class Circle {
    Point center;
    int radius;

    public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public double area() {
        return (Math.PI * radius * radius);
    }

    public double perimeter() {
        return (2 * Math.PI * radius);
    }

    public boolean intersect(Circle circle) {

        double xlen = center.xCoord - circle.center.xCoord;
        double ylen = center.yCoord - circle.center.yCoord;

        double hypo = Math.sqrt((xlen * xlen) + (ylen * ylen));

        return hypo < radius + circle.radius;
    }

    public static void main(String[] args) {
        Point cor1 = new Point(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
        int radius = Integer.parseInt(args[3]);
        Circle circobj = new Circle(cor1, radius);
    }

}
