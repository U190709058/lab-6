public class Main {
    public static void main(String[] args) {
        int x1 = Integer.parseInt(args[0]);
        int y1 = Integer.parseInt(args[1]);
        int x2 = Integer.parseInt(args[2]);
        int y2 = Integer.parseInt(args[3]);

        Point sideA =  new Point(x1, y1);
        Point sideB =  new Point(x2, y2);

        Rectangle testrect = new Rectangle(sideA, sideB);

        System.out.println(testrect.area());
        System.out.println(testrect.perimeter());

        for (Point element: testrect.corners()) {
            System.out.println(element.xCoord + " - " + element.yCoord);
        }

        Circle circle1 = new Circle(sideA, 10);
        Circle circle2 = new Circle(sideB, 10);

        System.out.println(circle1.area());
        System.out.println(circle1.perimeter());
        System.out.println(circle1.intersect(circle2));

    }
}
