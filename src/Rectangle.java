public class Rectangle {
    Point topleft;
    Point topright;
    Point bottomleft;
    Point bottomright;
    int xlen;
    int ylen;

    public Rectangle(Point sideA, Point sideB) {
        this.topleft = sideA;
        this.topright = new Point(sideA.xCoord, sideB.yCoord);
        this.bottomleft = new Point(sideB.xCoord, sideA.yCoord);
        this.bottomright = sideB;

        xlen = sideA.xCoord - sideB.xCoord;
        if (xlen < 0) {
            xlen = -xlen;
        }
        ylen = sideA.yCoord - sideB.yCoord;
        if (ylen < 0) {
            ylen = -ylen;
        }
    }

    public int area() {
        return xlen * ylen;
    }

    public int perimeter() {
        return 2 * (xlen + ylen);
    }

    public Point[] corners() {
        Point[] cornerlist;
        cornerlist = new Point[]{topleft, topright, bottomleft, bottomright};
        return cornerlist;
    }

    public static void main(String[] args) {
        Point cor1 = new Point(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
        Point cor2 = new Point(Integer.parseInt(args[2]),Integer.parseInt(args[3]));
        Rectangle rectobj = new Rectangle(cor1, cor2);
    }

}
